#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Django settings for {{ project_name }} project.

For more information on this file, see
https://docs.djangoproject.com/en/stable/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/stable/ref/settings/
"""

import ast
import os

import dj_database_url
import dj_email_url

# Set custom environment Variables
ENV = os.environ


#
# Custom functions
#
def get_list(text):
    """ Split the bash environment varilables into list. """
    return [item.strip() for item in text.split(",")]


def get_bool_from_env(name, default_value):
    """ Get boolean values from environment variables. """
    if name in ENV:
        value = ENV[name]
        try:
            return ast.literal_eval(value)
        except ValueError as err:
            raise ValueError(
                "{} is an invalid value for {}".format(value, name)
            ) from err
    return default_value


#
# Django debug toolbar
# https://docs.djangoproject.com/en/stable/ref/settings/#internal-ips
#
INTERNAL_IPS = get_list(ENV["INTERNAL_IPS"])


#
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
#
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


#
# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/stable/howto/deployment/checklist/
#
# SECURITY WARNING: keep the secret key used in production secret!
#
SECRET_KEY = ENV["SECRET_KEY"]

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = get_bool_from_env("DEBUG", False)

#
# This is a list of valid fully-qualified domain names (FQDNs) for the NetBox
# server. NetBox will not permit write access to the server via any other
# hostnames. The first FQDN in the list will be treated as the preferred name.
# https://docs.djangoproject.com/en/stable/ref/settings/#allowed-hosts
#
ALLOWED_HOSTS = get_list(ENV["ALLOWED_HOSTS"])


#
# Specify one or more name and email address tuples representing
# {{ project_name }} administrators. These people will be notified of
# application errors (assuming correct email settings are provided).
# https://docs.djangoproject.com/en/stable/ref/settings/#admins
#
ADMINS = [tuple(get_list(os.environ["ADMINS"]))]
MANAGERS = ADMINS


#
# Application definition
#
INSTALLED_APPS = [
    # Django
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
]


MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    # 3rd Party Django-CSP
    "csp.middleware.CSPMiddleware",
]

if DEBUG:
    INSTALLED_APPS += [
        # Development | Debug
        "sslserver",
        "debug_toolbar",
    ]
    MIDDLEWARE += [
        # Django Debug Toolbar
        "debug_toolbar.middleware.DebugToolbarMiddleware",
    ]

ROOT_URLCONF = "{{ project_name }}.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [os.path.join(BASE_DIR, "templates"), ],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "{{ project_name }}.wsgi.application"


#
# Database
# https://docs.djangoproject.com/en/stable/ref/settings/#databases
#
DATABASES = dict()
DATABASES["default"] = dj_database_url.config(
    conn_max_age=600, ssl_require=False
)


#
# Password validation
# https://docs.djangoproject.com/en/stable/ref/settings/#auth-password-validators
#
AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttribute\
SimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLength\
Validator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPassword\
Validator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPassword\
Validator",
    },
]


#
# Internationalization
# https://docs.djangoproject.com/en/stable/topics/i18n/
#
LANGUAGE_CODE = "en-us"
TIME_ZONE = "UTC"
USE_I18N = True
USE_L10N = True
USE_TZ = True


#
# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/stable/howto/static-files/
#
STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
]

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "staticfiles"),
]

STATIC_ROOT = os.path.join(BASE_DIR, "static")
STATIC_URL = "/static/"

# MEDIA_ROOT = os.path.join(BASE_DIR, "media")
# MEDIA_URL = "/media/"


#
# Cookie settings
# https://docs.djangoproject.com/en/stable/ref/settings/#sessions
#
SESSION_COOKIE_NAME = "X-SESSION-COOKIE"
SESSION_COOKIE_SAMESITE = "Lax"
SESSION_COOKIE_SECURE = True
SESSION_EXPIRE_AT_BROWSER_CLOSE = True


#
# CSRF Cookie Settings
# https://docs.djangoproject.com/en/stable/ref/settings/#csrf-cookie-httponly
#
CSRF_COOKIE_NAME = "X-CSRF-COOKIE"
CSRF_COOKIE_SAMESITE = "Lax"
CSRF_COOKIE_SECURE = True
CSRF_COOKIE_HTTPONLY = True
CSRF_TRUSTED_ORIGINS = ALLOWED_HOSTS

#
# HTTPS Everywhere
#
SECURE_SSL_REDIRECT = True
SECURE_SSL_HOST = ALLOWED_HOSTS
SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")

#
# Strict-Transport-Security
#
SECURE_HSTS_SECONDS = 31536000
SECURE_HSTS_INCLUDE_SUBDOMAINS = True
SECURE_HSTS_PRELOAD = True

#
# X-Frame-Options
# https://docs.djangoproject.com/en/stable/ref/settings/#x-frame-options
#
X_FRAME_OPTIONS = "DENY"

#
# X-XSS-Protection
# https://docs.djangoproject.com/en/stable/ref/settings/#secure-browser-xss-filter
#
SECURE_BROWSER_XSS_FILTER = True

#
# X-Content-Type-Options
#
SECURE_CONTENT_TYPE_NOSNIFF = True


#
# Content Security Policy (CSP)
#
CSP_DEFAULT_SRC = (
    "'self'",
    "http:",
    "https:",
    "data:",
    "blob:",
    "'unsafe-inline'",
)
CSP_STYLE_SRC = (
    "'self'",
    "'unsafe-inline'",
)
CSP_SCRIPT_SRC = ("'self'",)
CSP_FONT_SRC = ("'self'",)
CSP_IMG_SRC = ("'self'",)


#
# Email Settings
# https://docs.djangoproject.com/en/stable/ref/settings/#email-backend
#
email_config = dj_email_url.config()
EMAIL_FILE_PATH = email_config["EMAIL_FILE_PATH"]
EMAIL_HOST_USER = email_config["EMAIL_HOST_USER"]
EMAIL_HOST_PASSWORD = email_config["EMAIL_HOST_PASSWORD"]
EMAIL_HOST = email_config["EMAIL_HOST"]
EMAIL_PORT = email_config["EMAIL_PORT"]
EMAIL_BACKEND = email_config["EMAIL_BACKEND"]
EMAIL_USE_TLS = email_config["EMAIL_USE_TLS"]
EMAIL_USE_SSL = email_config["EMAIL_USE_SSL"]
EMAIL_SUBJECT_PREFIX = "[PROJECT] "

#
# Default sent email address
# https://docs.djangoproject.com/en/stable/ref/settings/#default-from-email
#
DEFAULT_FROM_EMAIL = ENV["DEFAULT_FROM_EMAIL"]
